# HSP_Aufgabensammlung_Einstieg
Von hier erhalten Sie einen Überblick über die Aufgabensammung und erhalten über die "invitation links" den Zugang zu den Aufgaben.

# Info
Der main Branch der jeweiligen aufgaben ist in .NET 5 sie können die jeweilige version ändern indem sie einfach den jeweiligen Branch auswählen

## Inhaltsverzeichnis:
0. [Level 0](#level-0) Für die Erklärung der Aufgabensammlung
1. [Level 1](#level-1) Anfänger
2. [Level 2](#level-2) Fortgeschrittene
3. [Level 3](#level-3) 
4. [Level 4](#level-4) Pro

## Level 0
[Level_0_HelloFunction](https://gitlab.gwdg.de/mecke/hsp-aufgabe-hellofunction):
Ganz einfache Aufgabe um die Funktion der Aufgabensammlung zu verstehen. Eine einfache Rückgabe eines Strings ist notwendig.

[Level 0 Addition Zweier Variablen](https://gitlab.gwdg.de/mecke/hsp-aufgabe-rechner):
Addieren sie zwei Variablen

## Level 1
[Level_1_HelloFunctionIF](https://gitlab.gwdg.de/mecke/hsp-aufgabe-hellofunctionif):
Erweitern Sie die HelloFunction mit einer Fallunter für klein- oder GROSS- Schreibung des String.

[Level_1_ObjektorientierteKreise](https://gitlab.gwdg.de/mecke/hsp-aufgabe-ookreise):
Ergänzen Sie, durch ToDos geführt, den Code für eine erste objektorientierte Anwendung.

[Level_1_Berechnung](https://gitlab.gwdg.de/mecke/hsp-aufgabe-berechnung):
Ergänzen Sie vier Berechnungsmethoden inkl. Ausgabe.

## Level 2
[Level_2_BubbleSort](https://gitlab.gwdg.de/mecke/hsp-aufgabe-bubblesort): 
Sortiere eine Liste mit diesem einfachen Sortierverfahren, welches immer die nächsten Nachbarn vergleicht und ggf. vertauscht. 
Voraussetzung ist das Arbeiten mit Listen (List) in C#.

[Level_2_Fakultaet](https://gitlab.gwdg.de/mecke/hsp-aufgabe-fakultaet):
Errechne in einer Methode die Fakultaet zu einer beliebigen Zahl.

[Level_2_Listen](https://gitlab.gwdg.de/mecke/hsp-aufgabe-listen):
Legen sie ein liste an und Fügen sie drei Strings in diese ein.

[Level_2_Summe bis n](https://gitlab.gwdg.de/mecke/hsp-aufgabe-summe_bis_n):
Errechne in einer Methode die Summe bis zu einer beliebigen Zahl. (1+2+3+4+5...+n)

[Level_2_Verdoppeln](https://gitlab.gwdg.de/mecke/hsp-aufgabe-verdoppeln):
Verdopple eine ganze Zahl, bis diese dreistellig wird und gib die Anzahl der nötigen Schritte aus.

## Level 3
[Level_3_Fibonacci](https://gitlab.gwdg.de/mecke/hsp-aufgabe-fibonacci):
Errechne in einer Methode die Fibonacci Folge und gebe diese bis zu einer beliebig positiven Zahl aus. 

[Level 3 MergeSort](https://gitlab.gwdg.de/mecke/hsp-aufgabe-mergesort):
Sortiere Array mit diesem Sortierverfahren, Voraussetzung ist das Arbeiten mit Arrays ([]) in C#.

[Level_3_Trigonometrie](https://gitlab.gwdg.de/mecke/hsp-aufgabe-trigonometrie):
Errechne in einer Methode die Seitenlängen, Winkel sowie den Umfang und die Fläche eines beliebigen Dreiecks. 

## Level 4
[Level_4_Türme von Hannoi](https://gitlab.gwdg.de/mecke/hsp-aufgabe-tuerme_von_hannoi):
Bringen sie drei "Scheiben" von einem "Turm" auf einen anderen ohne das eine "Scheibe" auf keinem "Turm" liegt oder eine größere "Scheibe" auf einer kleineren liegt.


## Hinweis
Wenn Sie feststellen, 
- dass ein Unit-Test nicht funktioniert, 
- oder dass die Vorlage für das Repo nicht funktioniert 
- oder dass eine Aufgabenstellung keinen Sinn macht, 
- oder anderes...

dann korrigieren Sie dies bitte in Ihrem persönlichen Repo und machen Sie uns auf Ihre Erkenntnis aufmerksam. Ihre Mitstudierenden werden es Ihnen danken.
